﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudienceManager : MonoBehaviour
{
  
    // Start is called before the first frame update
    private bool dirRight = true;
     public float speed = 1.0f;
 
     void Update () {
         if (dirRight)
             transform.Translate (Vector2.right * speed * Time.deltaTime);
         else
             transform.Translate (-Vector2.right * speed * Time.deltaTime);
         
         if(transform.position.x >= 1.0f) {
             dirRight = false;
         }
         
         if(transform.position.x <= 0.1) {
             dirRight = true;
         }
     }
}
