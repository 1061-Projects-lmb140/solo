﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudienceManager2 : MonoBehaviour
{
    // Start is called before the first frame update
 private bool dirRight = true;
     public float speed = 1.0f;
 
     void Update () {
         if (dirRight)
             transform.Translate (Vector2.right * speed * Time.deltaTime);
         else
             transform.Translate (-Vector2.right * speed * Time.deltaTime);
         
         if(transform.position.x >= 19.0f) {
             dirRight = false;
         }
         
         if(transform.position.x <= 18.1) {
             dirRight = true;
         }
     }
}
