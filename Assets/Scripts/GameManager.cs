﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using UnityEngine.SceneManagement;

// NEED TO IMPORT TEXT MESH PRO
// using TMPro;

public class GameManager : MonoBehaviour
{
    // Please make all of the instruments children of the GameManager object

    public static GameManager gm; // Singleton! :)

    public bool debug = false;

    public bool twoTargets;

    public int nNextTargets = 2; // controls how many targets are generated next round

    // Storing current target can cause problems if we want multiple targets at once
    //public GameObject target; // Could store the current target if necessary

    public InstrumentManager[] instruments; // All the children

    public int audienceReaction = 30;

    public InstrumentManager nextTarget;
    public InstrumentManager currentTarget;

//    public InstrumentManager nextTargetInstrument;

    // public TextMeshProUGUI audienceReactionText;

    private int maxReaction = 100;
    private int minReaction = 0;

    public int successBenefit = 10;
    public int failDetriment = 5;

    private GameObject audienceReactionBarObject;
    private Image audienceReactionBar;

    private Color goodColor = new Color(0f, 1f, 0f, 0.5f);
    private Color neutralColor = new Color(1f, 1f, 0f, 0.5f);
    private Color badColor = new Color(1f, 0f, 0f, 0.5f);

    public float sectionTime;
    public float deltaTime = 20;

    public float minDeltaTime;

    public float doubleChance = 0.5f;

    // Get audience UI elements in awake (avoid too many public vars)
    void Awake()
    {
        gm = gameObject.GetComponent<GameManager>();

        // NOTE: All instruments are children of the GameManager object
        instruments = gameObject.GetComponentsInChildren<InstrumentManager>();

        audienceReactionBarObject = GameObject.Find("AudienceReactionBar");
        audienceReactionBar = audienceReactionBarObject.GetComponent<Image>();

        // Get PlayerPref values
        deltaTime = PlayerPrefs.GetFloat("DeltaTime");
        doubleChance = PlayerPrefs.GetFloat("DoubleChance");

        minDeltaTime = deltaTime - (deltaTime / 5);
    }

    // Start is called before the first frame update
    void Start()
    {
        sectionTime = Time.time;

        // we need to do this twice, first to set the next targets, then to make them the active targets and set new ones.
        NextTarget();
        NextTarget();

        // Initially set reaction bar
        ChangeReactionBar();
    }

    // Selects a new target for the player and makes the audience reaction more positive
    public void SuccessfulHit()
    {
        audienceReaction += successBenefit;

        // Cap audience reaction at 100
        if (audienceReaction > maxReaction)
        {
            audienceReaction = maxReaction;

            // For now, maxReaction ends the game
            EndGame(true);
        }

        // NextTarget(); // Not present anymore because next target is a timed event now
        ChangeReactionBar();
    }

    //right now is an alias for failure, but could be more
    public void PartialHit()
    {
        FailedHit();
    }

    // helper method for the above, picks an instrument randomly and calls its change target
    // deprecated
    // public void NewTarget()
    // {
    //     //int index = Random.Range(0, instruments.Length);

    //     //instruments[index].ChangeTarget();
    // }

    public void NextTarget()
    {

        // unless the nexttarget is not set
        // set the next target to be the current target
        // Debug.Log(nextTarget);
        if(nextTarget != null)
        {
            nextTarget.MakeTarget();
            currentTarget = nextTarget;
        }

        // pick a random instrument to be the next target
        int index = Random.Range(0, instruments.Length);

        nextTarget = instruments[index];
        nextTarget.ChangeNextTarget();

    }

    // Makes the audience more mad and calls end of game if
    // their reaction drops below zero
    public void FailedHit()
    {
        audienceReaction -= failDetriment;

        if (audienceReaction < minReaction)
        {
            EndGame(false);
        }

        ChangeReactionBar();
    }

    // Takes in a boolean whether the player won or not, displays a message and offers new game
    // Details TBD
    public void EndGame(bool win)
    {
        if (win)
        {
            // display a cool thing

            // Temporarily a less than cool thing
            // GameObject.Find("VictoryMessage").GetComponent<CanvasGroup>().alpha = 1f;

            // Other than convenience, there isn't a strong reason for these to be scenes
            // Anyway, 2 is success and 3 is defeat
            SceneManager.LoadScene(2);
        }
        else
        {
            // display a loss state

            // TEMPORARY:
            // GameObject.Find("DefeatMessage").GetComponent<CanvasGroup>().alpha = 1f;
            SceneManager.LoadScene(3);
        }

        //Offer Restart or Quit
    }

    // Returns the section the song is in so that the appropriate chords may be played by tonal instruments
    // For demo purposes, returns 0 - the first selection in the "successClips" index
    public int GetSongSection()
    {
        return (0);
    }

    void IncreaseDifficulty()
    {
        if (deltaTime >= minDeltaTime) {
            deltaTime -= deltaTime / 40;
        }
        // Only increase doubleChance on medium and hard
        if (doubleChance > 0 && doubleChance < 0.98)
        {
            doubleChance += doubleChance / 40;
        }
    }

    void NextSongSection()
    {
        currentTarget.Deactivate();
        NextTarget();
        FailedHit();

        IncreaseDifficulty();
    }

    public void ForceNextSongSection()
    {
        currentTarget.Deactivate();
        NextTarget();
        sectionTime = Time.time;

        IncreaseDifficulty();
    }

    // Update is called once per frame
    void Update()
    {
        // Tack the audience reaction to the corresponding text
        // (We will probably want a bar or something later)
        // audienceReactionText.text = "Audience Reaction: " + audienceReaction;
        if (debug)
        {
            if (Input.GetKeyDown(KeyCode.D))
            {
                ForceNextSongSection();
            }
        }

        if(Time.time - sectionTime > deltaTime)
        {
            NextSongSection();

            sectionTime = Time.time;
        }

        // Escape to close game for now
        if (Input.GetKey("escape"))
        {
            // Doesn't do anything while testing
            Application.Quit();
        }
    }

    void ChangeReactionBar()
    {
        audienceReactionBarObject.transform.localScale = new Vector3(audienceReaction / 100.0f, 1, 1);

        if (audienceReaction >= 70)
        {
            audienceReactionBar.color = goodColor;
        }
        else if (audienceReaction <= 30)
        {
            audienceReactionBar.color = badColor;
        }
        else {
            audienceReactionBar.color = neutralColor;
        }
    }
}
