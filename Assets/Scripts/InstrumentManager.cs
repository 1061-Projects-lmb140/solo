﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstrumentManager : MonoBehaviour
{
    // Please make sure all of the triggers for an instrument are a child of that instrument (to which this script is attached)

    public SoundManager sm;

    public TriggerBehavior[] contactPoints;

    public List<TriggerBehavior> nextPoints;

    private void Awake()
    {
        // Grab all the relevant scripts from the children
        sm = gameObject.GetComponentInChildren<SoundManager>();
        contactPoints = gameObject.GetComponentsInChildren<TriggerBehavior>();
    }

    // Picks a new target from the instrument's children randomly and calls their target making script
    public void MakeTarget()
    {
        if(nextPoints.Count < 2)
        {
            GameManager.gm.twoTargets = false;
        }
        else
        {
            GameManager.gm.twoTargets = true;
        }

        foreach (TriggerBehavior i in nextPoints)
        {
            i.MakeTarget();
        }

        nextPoints.Clear();

    }

    public void Deactivate()
    {
        foreach(TriggerBehavior i in contactPoints)
        {
            if (i.isTarget)
            {
                i.UnmakeTarget();
            }

        }
    }

    public void ChangeNextTarget()
    {
        int loop = 0;
        int index;
        int tries = 0;
        TriggerBehavior selected;
        string hand = "both";

        // Make sure casual never gets two simultaneous triggers
        bool useTwoTriggers = Random.Range(0.01f, 1) < GameManager.gm.doubleChance;

        int loopStoppingPoint = 1;

        if (useTwoTriggers)
        {
            loopStoppingPoint = 2;
        }

        // We don't use nNextTargets, at the moment
        // while (loop < GameManager.gm.nNextTargets)
        while (loop < loopStoppingPoint)
        {
            index = Random.Range(0, contactPoints.Length);
            selected = contactPoints[index];

            if (selected.MakeNextTarget(hand))
            {
                nextPoints.Add(selected);

                if(loop > 1) // if this is more than the second iteration, the assumption is that both hands have a valid target already
                {
                    hand = "both";
                }
                else
                {
                    hand = selected.hand;
                }

                loop += 1;
            }
            tries += 1;

            // This is a loop break in case it can't get a lock because there aren't enough available spots on the instrument
            if(tries > 50)
            {
                return;
            }
        }
    }

    private void Update()
    {

    }
}
