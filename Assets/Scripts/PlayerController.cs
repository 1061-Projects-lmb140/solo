﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Make the player a singleton, why ever not?
    public static PlayerController instance;

    public GameObject currentCamera;

    public GameObject rightForeArm;
    public GameObject rightBackArm;

    public GameObject rightHand;

    public GameObject leftForeArm;
    public GameObject leftBackArm;

    public GameObject leftHand;

    private Rigidbody2D rightForeArmRigid;
    private Rigidbody2D rightBackArmRigid;

    private SpriteRenderer rightHandSprite;

    private Rigidbody2D leftForeArmRigid;
    private Rigidbody2D leftBackArmRigid;

    private SpriteRenderer leftHandSprite;

    public float rotateSpeed = 100f;

    public float cameraSpeed = 1000f;

    // Time when the movement started
    private float startTime;

    // Total distance
    private float journeyLength = 20f;

    private int cameraMovement = 0;
    private Vector3 destinationLocation;
    private Vector3 startingLocation;

    private Vector3 cameraDestinationLocation;
    private Vector3 cameraStartingLocation;


    private int currentInstrumentIndex = 0;
    private Vector3 leftMoveVector = new Vector3(-20, 0, 0);
    private Vector3 rightMoveVector = new Vector3(20, 0, 0);

    // Something of a coding faux pas to do as such:
    public TriggerBehavior leftHandCurrentTrigger;
    public TriggerBehavior rightHandCurrentTrigger;

    void Awake()
    {
        // Kill any attempt to defy the singleton
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

        rightForeArmRigid = rightForeArm.GetComponent<Rigidbody2D>();
        rightBackArmRigid = rightBackArm.GetComponent<Rigidbody2D>();

        rightHandSprite = rightHand.GetComponent<SpriteRenderer>();

        leftForeArmRigid = leftForeArm.GetComponent<Rigidbody2D>();
        leftBackArmRigid = leftBackArm.GetComponent<Rigidbody2D>();

        leftHandSprite = leftHand.GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        Interact();
        Movement();
        MoveToNewInstrument();
    }

    // void FixedUpdate()
    // {
    //     Movement();
    //     Interact();
    // }

    void Movement()
    {
        // qwas control the left arm
        if (Input.GetKey("q"))
        {
            leftBackArmRigid.MoveRotation(leftBackArmRigid.rotation + rotateSpeed * Time.deltaTime);
        }
        if (Input.GetKey("w"))
        {
            leftForeArmRigid.MoveRotation(leftForeArmRigid.rotation + rotateSpeed * Time.deltaTime);
        }
        if (Input.GetKey("a"))
        {
            leftBackArmRigid.MoveRotation(leftBackArmRigid.rotation - rotateSpeed * Time.deltaTime);
        }
        if (Input.GetKey("s"))
        {
            leftForeArmRigid.MoveRotation(leftForeArmRigid.rotation - rotateSpeed * Time.deltaTime);
        }

        // erdf control the right
        if (Input.GetKey("o"))
        {
            rightBackArmRigid.MoveRotation(rightBackArmRigid.rotation - rotateSpeed * Time.deltaTime);
        }
        if (Input.GetKey("p"))
        {
            rightForeArmRigid.MoveRotation(rightForeArmRigid.rotation - rotateSpeed * Time.deltaTime);
        }
        if (Input.GetKey("k"))
        {
            rightBackArmRigid.MoveRotation(rightBackArmRigid.rotation + rotateSpeed * Time.deltaTime);
        }
        if (Input.GetKey("l"))
        {
            rightForeArmRigid.MoveRotation(rightForeArmRigid.rotation + rotateSpeed * Time.deltaTime);
        }
    }

    void MoveToNewInstrument()
    {
        // For moving between instruments
        if (cameraMovement == 0)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                // Debug.Log("MOVE LEFT");
                // No going left at drums
                if (currentInstrumentIndex > 0)
                {
                    // transform.Translate(leftMoveVector);
                    // currentCamera.transform.Translate(leftMoveVector);

                    startingLocation = transform.position;
                    destinationLocation = startingLocation + leftMoveVector;

                    cameraStartingLocation = currentCamera.transform.position;
                    cameraDestinationLocation = cameraStartingLocation + leftMoveVector;

                    startTime = Time.time;

                    cameraMovement = 1;

                    currentInstrumentIndex -= 1;
                }
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                // Debug.Log("MOVE RIGHT");
                if (currentInstrumentIndex < 2)
                {
                    // transform.Translate(rightMoveVector);
                    // currentCamera.transform.Translate(rightMoveVector);

                    startingLocation = transform.position;
                    destinationLocation = startingLocation + rightMoveVector;

                    cameraStartingLocation = currentCamera.transform.position;
                    cameraDestinationLocation = cameraStartingLocation + rightMoveVector;

                    startTime = Time.time;

                    cameraMovement = 1;

                    currentInstrumentIndex += 1;
                }
            }
        }
        // Not else if because cameraMovement set above
        if (cameraMovement == 1)
        {
            // Distance moved equals elapsed time times speed
            float distCovered = (Time.time - startTime) * cameraSpeed;

            // Fraction of journey completed equals current distance divided by total distance
            float fractionOfJourney = distCovered / journeyLength;

            // Set our position as a fraction of the distance between the markers
            transform.position = Vector3.Lerp(startingLocation, destinationLocation, fractionOfJourney);
            currentCamera.transform.position = Vector3.Lerp(cameraStartingLocation, cameraDestinationLocation, fractionOfJourney);

            // When close enough, snap
            if (fractionOfJourney > 0.98)
            {
                transform.position = destinationLocation;
                currentCamera.transform.position = cameraDestinationLocation;
                cameraMovement = 0;
            }
        }
    }

    void Interact()
    {
        void Success()
        {
            GameManager.gm.SuccessfulHit();

            if (leftHandCurrentTrigger)
            {
                leftHandCurrentTrigger.PlaySuccessSound();
            }
            else {
                rightHandCurrentTrigger.PlaySuccessSound();
            }

            // It is very boring having to wait for the next target after you have a successful hit
            // This problem is only made worse by changing the deltaTime with the difficulty!
            // For the moment, a success cycles a new target
            GameManager.gm.ForceNextSongSection();

            // GameManager.gm.currentTarget.Deactivate();
        }

        void PartialSuccess()
        {
            // remember that as of right now, both of these are aliases for failure
            // leftHandCurrentTrigger.PlayPartialSuccessSound();

            if (leftHandCurrentTrigger)
            {
                leftHandCurrentTrigger.PlayPartialSuccessSound();
            }
            else {
                rightHandCurrentTrigger.PlayPartialSuccessSound();
            }

            GameManager.gm.PartialHit();
        }

        void Failure()
        {
            // leftHandCurrentTrigger.PlayFailureSound();

            if (leftHandCurrentTrigger)
            {
                leftHandCurrentTrigger.PlayFailureSound();
            }
            else {
                rightHandCurrentTrigger.PlayFailureSound();
            }

            GameManager.gm.FailedHit();
        }


        // Space activates both arms for now
        // For ease of dealing with triggers, interact lasts only one frame
        if (Input.GetKeyDown("space"))
        {
            leftHandSprite.color = Color.red;
            rightHandSprite.color = Color.red;


            if (leftHandCurrentTrigger && rightHandCurrentTrigger)
            {
                if(leftHandCurrentTrigger.isTarget && rightHandCurrentTrigger.isTarget)
                {
                    Success();
                }
                else if (leftHandCurrentTrigger.isTarget || rightHandCurrentTrigger.isTarget)
                {
                    // if there is only one target, count this as a success?
                    if (GameManager.gm.twoTargets)
                    {
                        PartialSuccess();
                    }
                    else
                    {
                        Success();
                    }

                }
                else
                {
                    Failure();
                }

            }
            else if (leftHandCurrentTrigger || rightHandCurrentTrigger)
            {
                if((leftHandCurrentTrigger && leftHandCurrentTrigger.isTarget) || (rightHandCurrentTrigger && rightHandCurrentTrigger.isTarget))
                {
                    if (GameManager.gm.twoTargets)
                    {
                        PartialSuccess();
                    }
                    else
                    {
                        Success();
                    }
                }
                else
                {
                    Failure();
                }
            }

        }
        else {
            leftHandSprite.color = Color.white;
            rightHandSprite.color = Color.white;
        }
    }

    public void SetCurrentLeftTrigger(string currentTrigger, TriggerBehavior triggerToSet)
    {
        if (triggerToSet)
        {
            leftHandCurrentTrigger = triggerToSet;
        }
        else if (!triggerToSet && leftHandCurrentTrigger && leftHandCurrentTrigger.gameObject.name == currentTrigger)
        {
            leftHandCurrentTrigger = null;
        }
    }

    public void SetCurrentRightTrigger(string currentTrigger, TriggerBehavior triggerToSet)
    {
        if (triggerToSet)
        {
            rightHandCurrentTrigger = triggerToSet;
        }
        else if (!triggerToSet && rightHandCurrentTrigger && rightHandCurrentTrigger.gameObject.name == currentTrigger)
        {
            rightHandCurrentTrigger = null;
        }
    }
}
