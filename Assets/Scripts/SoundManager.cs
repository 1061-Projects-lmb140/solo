﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    // These are meant to be changed in the editor
    public AudioClip[] successClips; // Each section has one success clip at its index
    public AudioClip[] failClips; // In no particular order

    AudioSource player;

    // Start is called before the first frame update
    void Awake()
    {
        player = gameObject.GetComponent<AudioSource>();

        // Debug.Log(successClips.Length);
    }

    void KillAudienceSound()
    {
        // REVISE IF TIME PERMITS
        // Stop all audience audio
        GameObject.Find("AudienceReactionCheers").GetComponent<AudioSource>().Stop();
        GameObject.Find("AudienceReactionBoos").GetComponent<AudioSource>().Stop();
        // GameObject.Find("AudienceReactionCrickets").GetComponent<AudioSource>().Stop();
    }

    // Plays the success clip for this section of the song
    // Each section has one success clip at its index
    public void PlaySuccess()
    {
        KillAudienceSound();

        // We use random success sounds at the moment
        // int index = GameManager.gm.GetSongSection();

        int index = Random.Range(0, successClips.Length);

        if (player.isPlaying)
        {
            player.Stop();
        }

        player.clip = successClips[index];

        player.Play();

        // Delay play cheers
        GameObject.Find("AudienceReactionCheers").GetComponent<AudioSource>().PlayDelayed(successClips[index].length);
    }

    // Plays a specific index of success
    // included for debug purposes mostly
    public void PlaySuccess(int index)
    {
        KillAudienceSound();

        //int index = GameManager.gm.GetSongSection();

        if (player.isPlaying)
        {
            player.Stop();
        }

        player.clip = successClips[index];

        player.Play();

        // Delay play cheers
        GameObject.Find("AudienceReactionCheers").GetComponent<AudioSource>().PlayDelayed(successClips[index].length);
    }

    // This model works better for the drums where each contact point should
    // make the same sound every time
    public void PlaySuccess(AudioClip audio)
    {
        KillAudienceSound();

        if (player.isPlaying)
        {
            player.Stop();
        }

        player.clip = audio;

        player.Play();

        // Delay play cheers
        GameObject.Find("AudienceReactionCheers").GetComponent<AudioSource>().PlayDelayed(player.clip.length);
    }

    // Stops the current sound playing and swaps to playing a random failure sound
    public void PlayFailure()
    {
        KillAudienceSound();

        int index = Random.Range(0, failClips.Length);

        if (player.isPlaying)
        {
            player.Stop();
        }

        player.clip = failClips[index];

        player.Play();

        // Delay play boos
        GameObject.Find("AudienceReactionBoos").GetComponent<AudioSource>().PlayDelayed(failClips[index].length);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
