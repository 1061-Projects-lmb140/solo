﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// NEED TO IMPORT TEXT MESH PRO
using TMPro;

public class StartMenu : MonoBehaviour
{
    private CanvasGroup tutorialCanvasGroup;

    private void Awake()
    {
        tutorialCanvasGroup = GameObject.Find("Tutorial").GetComponent<CanvasGroup>();

        if (PlayerPrefs.HasKey("DeltaTime") && PlayerPrefs.GetFloat("DeltaTime") == 60f)
        {
            SetCasual();
        }
        else if (PlayerPrefs.HasKey("DeltaTime") && PlayerPrefs.GetFloat("DeltaTime") == 15f) {
            SetHard();
        }
        else {
            SetMedium();
        }
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void ShowTutorial()
    {
        tutorialCanvasGroup.alpha = 1.0f;
        tutorialCanvasGroup.blocksRaycasts = true;
    }

    public void HideTutorial()
    {
        tutorialCanvasGroup.alpha = 0f;
        tutorialCanvasGroup.blocksRaycasts = false;
    }

    public void SetCasual()
    {
        PlayerPrefs.SetFloat("DeltaTime", 60f);
        PlayerPrefs.SetFloat("DoubleChance", 0.0f);

        GameObject.Find("DifficultyPrompt").GetComponent<TextMeshProUGUI>().text = "Current Difficulty: Casual";
    }

    public void SetMedium()
    {
        PlayerPrefs.SetFloat("DeltaTime", 20f);
        PlayerPrefs.SetFloat("DoubleChance", 0.5f);

        GameObject.Find("DifficultyPrompt").GetComponent<TextMeshProUGUI>().text = "Current Difficulty: Medium";
    }

    public void SetHard()
    {
        PlayerPrefs.SetFloat("DeltaTime", 15f);
        PlayerPrefs.SetFloat("DoubleChance", 0.95f);

        GameObject.Find("DifficultyPrompt").GetComponent<TextMeshProUGUI>().text = "Current Difficulty: Hard";
    }
}
