﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBehavior : MonoBehaviour
{

    public bool isTarget; // keeps track of whether or not this is the trigger the player is supposed to hi
    public bool isNextTarget;

    public string hand = "both"; // Should be one of [left, right, both]

    InstrumentManager instrument; // the parent instrument
    // For the moment (perhaps longer?) triggers are sprites
    // MeshRenderer mesh;

    public AudioClip sound; //in case you want this trigger to have a specific success sound different from the instrument's general bank

    // // These two are the materials for when the trigger is and is not the target
    // public Material onMaterial;
    // public Material offMaterial;

    private SpriteRenderer spriteRender;

    private Color onColor = new Color(0, 0.8f, 0, 0.25f);
    private Color offColor = new Color(0.8f, 0, 0, 0.25f);
    private Color nextColor = new Color(0.8f, 0, 0.8f, 0.25f);

    // Start is called before the first frame update
    void Awake()
    {
        // initialize...
        instrument = gameObject.GetComponentInParent<InstrumentManager>();

        // In the interest of time (and me breaking the store assets when messing with the shaders):
        // this will be done with sprites for the moment
        // mesh = gameObject.GetComponent<MeshRenderer>();

        spriteRender = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool MakeTarget()
    {


        if (isTarget)
        {
            return false;
        }
        if(isNextTarget)
        {
            isTarget = true;
            isNextTarget = false;

            // change the color to green or whatever
            // mesh.material = onMaterial;

            // Set to onColor apparently
            spriteRender.color = onColor;

            return (true);
        }
        else
        {
            return (false);
        }
    }

    public bool MakeNextTarget(string otherHand)
    {
        // isTarget = false;

        if(isTarget || isNextTarget)
        {
            return (false);
        }
        else
        {
            if(otherHand == null || otherHand == "both" || otherHand != hand)
            {
                //GameManager.gm.nextTarget = this; // no longer managed through the gm
                isNextTarget = true;
                // Set to zero to hide again
                spriteRender.color = nextColor;
                return (true);

            }
            else
            {
                return (false);
            }

        }

    }

    public void UnmakeTarget()
    {
        isTarget = false;

        //change back to transparent or red or what have you
        // mesh.material = offMaterial;

        // Set to zero to hide again
        spriteRender.color = offColor;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        // In lieu of resolving immediately, the trigger will just set values in the PlayerController
        // We use tags to indicate left vs. right hands
        // Only the hands interest us
        // This is to push the resolution of the triggers over to the player controller so that pushing spacebar

        if (other.gameObject.tag == "Left")
        {
            // Debug.Log("Left Enter: " + gameObject.name);
            // PlayerController.instance.leftHandCurrentTrigger = this;
            PlayerController.instance.SetCurrentLeftTrigger(gameObject.name, this);
        }
        if (other.gameObject.tag == "Right")
        {
            // Debug.Log("Right Enter: " + gameObject.name);
            // PlayerController.instance.rightHandCurrentTrigger = this;
            PlayerController.instance.SetCurrentRightTrigger(gameObject.name, this);
        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Left")
        {
            // Debug.Log("Left Exit: " + gameObject.name);
            // PlayerController.instance.leftHandCurrentTrigger = null;
            PlayerController.instance.SetCurrentLeftTrigger(gameObject.name, null);
        }
        if (other.gameObject.tag == "Right")
        {
            // Debug.Log("Right Exit: " + gameObject.name);
            // PlayerController.instance.rightHandCurrentTrigger = null;
            PlayerController.instance.SetCurrentRightTrigger(gameObject.name, null);
        }
    }

    public void PlaySuccessSound()
    {
        if (sound != null) // if this trigger has a special sound play it, otherwise pick one
        {
            instrument.sm.PlaySuccess(sound);
        }
        else
        {
            instrument.sm.PlaySuccess();
        }
    }

    public void PlayFailureSound()
    {
        instrument.sm.PlayFailure();
    }

    public void PlayPartialSuccessSound()
    {
        // change this if/when we do partial success
        // currently implemented to be an alias for PlayFailureSound

        PlayFailureSound();
    }

    // Sort of tricky! Hit resolution can depend on multiple!
    // NOTE: EXPOSED FOR NOW
    // NOTE 2: Deprecated for now
    public void ResolveHit()
    {
        if (isTarget) // if this is the right hit to make
        {
            UnmakeTarget();

            GameManager.gm.SuccessfulHit();

            PlaySuccessSound();



        }
        else // if it's a wrong hit
        {
            GameManager.gm.FailedHit();

            PlayFailureSound();
        }
    }
}
