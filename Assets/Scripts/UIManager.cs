﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private Color keyPromptOffColor = new Color(0.2431373f, 0.3176471f, 0.3921569f, 0.5019608f);
    private Color keyPromptOnColor = new Color(1f, 1f, 0.15f, 0.75f);

    private Dictionary<string, Image> keyPromptImages = new Dictionary<string, Image>();

    private string[] keyStrings = new string[] {
        "q",
        "w",
        "a",
        "s",
        "o",
        "p",
        "k",
        "l",
        "space"
    };

    // Start is called before the first frame update
    void Awake()
    {

        for (int i=0; i < keyStrings.Length; i++)
        {
            keyPromptImages.Add(keyStrings[i], GameObject.Find(keyStrings[i].ToUpper() + "Key").GetComponent<Image>());
        }
        // qKey = GameObject.Find("QKey").GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        for (int i=0; i < keyStrings.Length; i++)
        {
            // Somewhat harsh, but it works
            if (Input.GetKey(keyStrings[i]))
            {
                keyPromptImages[keyStrings[i]].color = keyPromptOnColor;
            }
            else {
                keyPromptImages[keyStrings[i]].color = keyPromptOffColor;
            }
        }
    }
}
