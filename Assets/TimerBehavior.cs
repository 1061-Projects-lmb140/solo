﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerBehavior : MonoBehaviour
{
    float timerValue;

    Text thisText;
    public GameObject subTextObject;
    Text subText;

    void Awake()
    {
        thisText = gameObject.GetComponent<Text>();
        subText = subTextObject.GetComponent<Text>();
    }

    int Expander(float input)
    {
        if(input > 6)
        {
            return (0);
        }
        else
        {
            float output = (6 - input) * 6;

            return ((int) output);
        }
    }

    // Update is called once per frame
    void Update()
    {
        timerValue = GameManager.gm.deltaTime - (Time.time - GameManager.gm.sectionTime);

        if(timerValue < 1)
        {
            thisText.text = timerValue.ToString("#.0");
            subText.text = timerValue.ToString("#.0");
        }
        else
        {
            thisText.text = timerValue.ToString("#");
            subText.text = timerValue.ToString("#");
        }



        thisText.fontSize = 72 + Expander(timerValue);
        subText.fontSize = 72 + Expander(timerValue);
    }
}
